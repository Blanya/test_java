package org.example;

import org.example.exceptions.ErrorWordException;

import static java.lang.String.valueOf;

public class LePendu {

    private String[] tableWords = {"acajou", "jouer", "adorer", "guerrier", "suer", "pseudonyme", "email", "chaise", "cahier", "tasse", "cigarette"};
    private MotRandom _randomWord;
    private String[] maskedWord;

    private int tentative = 10;


    public LePendu(MotRandom randomWord){
        _randomWord = randomWord;
    }

    public String[] getMaskedWord() {
        return maskedWord;
    }

    public void setMaskedWord(String[] maskedWord) {
        this.maskedWord = maskedWord;
    }

    public int getTentative() {
        return tentative;
    }

    public String[] maskWord() throws Exception{

        if(_randomWord.defineWord() == null || _randomWord.defineWord().equals("")) {
            throw new ErrorWordException();
        }

        else{
            maskedWord = new String[_randomWord.defineWord().length()];

            for (int i = 0; i < _randomWord.defineWord().length(); i++) {
                maskedWord[i] = "*";
            }

            return maskedWord;
        }
    }

    public boolean askLetter(String letter) throws Exception{
        letter = letter.toLowerCase();

        if(_randomWord.defineWord().contains(letter)){
            String[] proposition = new String[_randomWord.defineWord().length()];

            for (int i = 0; i < _randomWord.defineWord().length(); i++) {
                if (letter.equals(valueOf(_randomWord.defineWord().charAt(i)))) {
                    proposition[i] = letter;
                } else {
                    proposition[i] = maskedWord[i];
                }
            }

            maskedWord = proposition;
            return true;
        }
        --tentative;
        return _randomWord.defineWord().contains(letter);
    }

    public boolean findWord() throws Exception{
        String motTrouve = String.join("", maskedWord);

        return motTrouve.equals(_randomWord.defineWord()) && tentative>0;
    }

}
