package org.example;

import org.example.exceptions.NotFoundException;

import java.util.ArrayList;
import java.util.List;

public class City {

    //Pour mock => List ds la classTest
//    private List<String> cityList = Arrays.asList("Paris", "Budapest", "Skopje", "Rotterdam", "Valence", "Vancouver", "Amsterdam", "Vienne", "Sydney", "New York", "Londres", "Bangkok", "Hong Kong", "Dubaï", "Rome", "Istanbul");

    private List<String> cityList;

    public List<String> getCityList() {
        return cityList;
    }

    public void setCity(List<String> city) {
        this.cityList = city;
    }

    public List<String> getCity(String text) throws Exception{
        List<String> cities = new ArrayList<>();

        if(text.equals("*"))
            return cityList;
        else if(text.length()<2)
        {
            throw new NotFoundException();
        }
        else {
            for (String c: cityList) {
                if(c.toLowerCase().contains(text.toLowerCase()))
                {
                    cities.add(c);
                }
            }
            return cities;
        }
    }
}
