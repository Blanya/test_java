package org.example;

public class MotRandom {

    private String[] tableWords = {"acajou", "jouer", "adorer", "guerrier", "suer", "pseudonyme", "email", "chaise", "cahier", "tasse", "cigarette"};


    public String defineWord(){
        String randomWord = tableWords[getRandom(0, tableWords.length -1)];
        return randomWord.toLowerCase();
    }

    public static int getRandom(int min, int max)
    {
        return (int) Math.floor(Math.random() * (max - min) + min);
    }

}
