package org.example.exceptions;

public class NotEANException extends Exception{
    public NotEANException(){
        super("Erreur: code-barre introuvable");
    }
}
