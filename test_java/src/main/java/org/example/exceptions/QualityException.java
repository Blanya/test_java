package org.example.exceptions;

public class QualityException extends Exception{
    public QualityException(){
        super("Quality can't be negative");
    }
}
