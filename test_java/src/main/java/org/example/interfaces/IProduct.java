package org.example.interfaces;

import org.example.ProductSc;

import java.util.List;

public interface IProduct<T> {
    List<T> genererProduct();
}
