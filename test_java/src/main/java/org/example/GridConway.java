package org.example;

import org.example.interfaces.IGrid;

public class GridConway {
    // tableau 2 D
    private IGrid _igrid;
    public GridConway(IGrid iGrid){
        _igrid = iGrid;
    }

    public boolean[][] haveATurn() throws Exception{
        boolean[][] grid = _igrid.genereGrid();

        //Créer copie de grid
        boolean[][] gridCopy = new boolean[grid.length][];

        for(int i = 0; i < grid.length; i++) {
            gridCopy[i] =  new boolean[grid[i].length];
            System.arraycopy(grid[i], 0, gridCopy[i], 0, grid[i].length);
        }

        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[i].length; j++) {
                //Checker 8 autres case

                //Cases voisines vivantes
                int countAlive = 0;

                //définir nombre max -> doit être inférieur a la largeur et à la hauteur
                int kMax = i+1 < grid.length ?  i+1: i;
                int lMax = j+1 < grid[i].length ? j+1: j;

                for (int k = i>0 ?  i-1: 0; k <= kMax; k++) {
                    for (int l = j>0 ?  j-1: 0; l <= lMax; l++){
                        //Si on est sur la case
                        if(k == i && l == j)
                            continue;
                        //checker les voisins (cellule morte ou vivante)
                        else{
                               if(grid[k][l])
                                   ++countAlive;
                           }
                        }
                }
                if(grid[i][j] && (countAlive == 2 || countAlive == 3))
                    gridCopy[i][j] = true;
                else gridCopy[i][j] = !grid[i][j] && countAlive == 3;
            }
        }
        return gridCopy;
    }

}
