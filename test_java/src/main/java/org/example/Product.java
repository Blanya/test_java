package org.example;

import org.example.exceptions.QualityException;

import java.util.InputMismatchException;

public class Product {
    private int sellIn;
    private double quality;
    private String name;
    private String category;

    public Product() {
    }

    public Product(int sellIn, double quality, String name, String category) {
        this.sellIn = sellIn;
        this.quality = quality;
        this.name = name;
        this.category = category;
    }

    public int getSellIn() {
        return sellIn;
    }

    public void setSellIn(int sellIn) {
        this.sellIn = sellIn;
    }

    public double getQuality() {
        return quality;
    }

    public void setQuality(double quality) {
        this.quality = quality;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Product updateProduit() throws Exception{
        if(quality == 50)
            throw new InputMismatchException("Valeur maximale atteinte");
        else if(name.contains("vieilli"))
            setQuality(quality += 1);
        else if(category.equals("laitier") || sellIn <= 1)
            setQuality(this.quality/2);
        if (quality <= 0)
                throw new QualityException();
        setSellIn(sellIn-1);
        return this;
    }

}
