package org.example;

public class Year {

    public boolean checkLeapYear(int year){

//        if (year%4000 == 0)
//            return true;
//        else if(year%400 == 0)
//            return true;
//        else if(year%4 == 0 && year%100 != 0)
//            return true;
//        return false;

        return year%4000 == 0? true : year%400 == 0? true : (year%4 == 0 && year%100 != 0)? true : false;
    }
}
