package org.example;

import org.example.interfaces.IProduct;

import java.util.List;

public class ProductSc{
    private String codeBarre;
    private double price;

    public ProductSc(String codeBarre, double price) {
        this.codeBarre = codeBarre;
        this.price = price;
    }

    public ProductSc(String codeBarre) {
        this.codeBarre = codeBarre;
    }

    public ProductSc() {
    }

    public String getCodeBarre() {
        return codeBarre;
    }
    public double getPrice() {
        return price;
    }
}
