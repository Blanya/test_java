package org.example;

import org.example.exceptions.NotEANException;
import org.example.interfaces.IProduct;

import java.util.List;

public class ScannerProduct{

    private List<ProductSc> _productList;
    private IProduct _iProduct;
    public ScannerProduct(IProduct iProduct){
        _iProduct = iProduct;
    }
    public double scanProduct() throws Exception{
        _productList = _iProduct.genererProduct();
        double total = 0;

        for (ProductSc p: _productList) {
            if(p.getCodeBarre() == "99999")
                throw new NotEANException();
            else if (p.getCodeBarre().isBlank()) {
                throw new NullPointerException("Erreur: code-barre introuvable");
            }
            else {
                total += p.getPrice();
            }
        }

        return total;
    }
}
