package org.example;

import org.example.exceptions.QualityException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.InputMismatchException;

public class ProductTest {

    private Product product;

    @BeforeEach
    private void setUp(){
        product = new Product();
    }

    private Product actAndArrange(int sellIn, double quality, String name, String category){
        product.setSellIn(sellIn);
        product.setQuality(quality);
        product.setName(name);
        product.setCategory(category);
        return product;
    }

    @Test
    void testUpdateProduitShouldHaveQualityHalfAndSellInLessThan1() throws Exception {
        product = actAndArrange(1, 8, "jambon", "charcuterie");
        Assertions.assertEquals( 4, (product.updateProduit()).getQuality());
    }

    @Test
    void testUpdateProduitShouldNotHaveQualityNegative() throws Exception {
        product = actAndArrange(2, 0, "jambon", "charcuterie");
        Assertions.assertThrowsExactly(QualityException.class, () -> {
            product.updateProduit();
        });
    }

    @Test
    void testUpdateProduitShouldNotHaveQualityGT50() throws Exception {
        product = actAndArrange(3, 50, "brie vieilli", "laitier");
        //Exception
        Assertions.assertThrowsExactly(InputMismatchException.class, () -> product.updateProduit());
    }

    @Test
    void testUpdateProduitShouldIncreaseQualityIfProductContainsVieilli() throws Exception {
        product = actAndArrange(10, 8, "brie vieilli", "laitier");
        Assertions.assertEquals(9, product.updateProduit().getQuality());
    }

    @Test
    void testUpdateProduitShouldHaveQualityHalfIfProductCategoryIsLaitier() throws Exception {
        product = actAndArrange(4, 19, "yaourt", "laitier");
        Assertions.assertEquals(9.5, product.updateProduit().getQuality());
    }
}
