package org.example;

import org.example.exceptions.NotFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

public class CityTest {
    private City city;
    private List<String> cities;

    @BeforeEach
    void setUp(){
        city = new City();
//        cities = city.getCityList();
//        Mock =>
        city.setCity(Arrays.asList("Paris", "Budapest", "Skopje", "Rotterdam", "Valence", "Vancouver", "Amsterdam", "Vienne", "Sydney", "New York", "Londres", "Bangkok", "Hong Kong", "Dubaï", "Rome", "Istanbul"));
        cities = city.getCityList();
    }

//    @Test
//    void testShouldBeNullWhenTextSizeLessThan2()
//    {
//        Assertions.assertNull(city.getCity("P"));
//    }

    @Test
    void rechercherShouldRaiseExceptionWhenMotIslessThen2chars() {
        Assertions.assertThrowsExactly(NotFoundException.class, () -> {
            city.getCity("a");
        });
    }

    @Test
    void testShouldBeListWithValenceAndVancouverWhenTextIsVa() throws Exception {
        Assertions.assertEquals(Arrays.asList("Valence", "Vancouver"), city.getCity("Va"));
    }

    @Test
    void testShouldBeListWithValenceAndVancouverWhenTextIsVA() throws Exception {
        Assertions.assertEquals(Arrays.asList("Valence", "Vancouver"), city.getCity("VA"));
    }

    @Test
    void testShouldBeBudapestWhenTextIsUda() throws Exception {
        Assertions.assertEquals(Arrays.asList("Budapest"), city.getCity("uda"));
    }

    @Test
    void testShouldBeAllCitiesWhenTextIsAst() throws Exception {
        Assertions.assertEquals(cities, city.getCity("*"));
    }

}
