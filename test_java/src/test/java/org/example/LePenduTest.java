package org.example;

import org.example.exceptions.ErrorWordException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class LePenduTest {


    @Mock //pour donner une valeur définie
    private MotRandom motRandom;

    private LePendu lePendu;

    @BeforeEach
    void setUp() throws Exception {
        lePendu = new LePendu(motRandom);
        Mockito.when(motRandom.defineWord()).thenReturn("petit");
        lePendu.maskWord();
    }

    @Test
    void testMaskWordShouldBeCorrectWithRandomWordPetit() throws Exception {
        Assertions.assertArrayEquals(new String[]{"*", "*", "*", "*", "*"}, lePendu.maskWord());
    }

    @Test
    void MaskWordShouldRaiseExceptionIfNoChars() throws Exception{
        Mockito.when(motRandom.defineWord()).thenReturn("");

        Assertions.assertThrowsExactly(ErrorWordException.class, () -> {
            lePendu.maskWord();
        });
    }

    @Test
    void MaskWordShouldRaiseExceptionIfRandomWordIsNull() throws Exception{
        Mockito.when(motRandom.defineWord()).thenReturn(null);

        Assertions.assertThrowsExactly(ErrorWordException.class, () -> {
            lePendu.maskWord();
        });
    }

    @Test
    void testAskLetterShouldBeTrueIfLetterIsInWord() throws Exception {
        //A
//        lePendu.maskWord();
        Assertions.assertTrue(lePendu.askLetter("e"));
    }

    @Test
    void testAskLetterShouldBeFalseIfLetterIsNotInWord() throws Exception {
        //A
//        lePendu.maskWord();
        Assertions.assertFalse(lePendu.askLetter("a"));
    }

    @Test
    void testAskLetterShouldMaskChangeIfGoodLetter() throws Exception {
//        lePendu.maskWord();
        lePendu.askLetter("e");
        lePendu.askLetter("t");
        Assertions.assertArrayEquals(new String[]{"*", "e", "t", "*", "t"}, lePendu.getMaskedWord());
    }

    @Test
    void testAskLetterShouldMaskNotChangeIfWrongLetter() throws Exception {
//        lePendu.maskWord();
        lePendu.askLetter("p");
        lePendu.askLetter("a");
        Assertions.assertArrayEquals(new String[]{"p", "*", "*", "*", "*"}, lePendu.getMaskedWord());
    }

    @Test
    void testAskLetterShouldTentativeLessThan1WhenLetterIsWrong() throws Exception {
//        lePendu.maskWord();
        lePendu.askLetter("m");
        Assertions.assertEquals(9, lePendu.getTentative());
    }

    @Test
    void testAskLetterShouldTentativeEquals10WithGoodLetter() throws Exception {
//        lePendu.maskWord();
        lePendu.askLetter("e");
        Assertions.assertEquals(10, lePendu.getTentative());
    }

    @Test
    void testShouldBeTrueIfGoodWorld() throws Exception{
        //a
        lePendu.askLetter("e");
        lePendu.askLetter("t");
        lePendu.askLetter("P");
        lePendu.askLetter("i");
        Assertions.assertTrue(lePendu.findWord());
    }

    @Test
    void testShouldBeFalseIfWrongWorld() throws Exception{
        Assertions.assertFalse(lePendu.findWord());
    }
}
