package org.example;

import org.example.exceptions.NotEANException;
import org.example.interfaces.IProduct;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.List;

@ExtendWith(MockitoExtension.class)
public class ScannerProductTest {
    private ScannerProduct scannerProduct;

    @Mock
    private IProduct iProduct;


    void arrangeAncAct(List productList){
        scannerProduct = new ScannerProduct(iProduct);
        Mockito.when(iProduct.genererProduct()).thenReturn(productList);
    }

    @Test
    void testScanProductWithEAN12345ShouldBe7e25() throws Exception {
        arrangeAncAct(Arrays.asList(new ProductSc("12345", 7.25)));
        Assertions.assertEquals(7.25, scannerProduct.scanProduct());
    }

    @Test
    void testScanProductWithEAN23456ShouldBe12e50() throws Exception {
        arrangeAncAct(Arrays.asList(new ProductSc("23456", 12.50)));
        Assertions.assertEquals(12.50, scannerProduct.scanProduct());
    }

    @Test
    void testScanProductWithEAN99999ShouldRaiseNotFoundException() throws Exception {
        arrangeAncAct(Arrays.asList(new ProductSc("99999")));
        Assertions.assertThrowsExactly(NotEANException.class, () -> scannerProduct.scanProduct());
    }

    @Test
    void testScanProductWithEmptyEANShouldRaiseException() throws Exception {
        arrangeAncAct(Arrays.asList(new ProductSc()));
        Assertions.assertThrowsExactly(NullPointerException.class, () -> scannerProduct.scanProduct());
    }

    @Test
    void testScanProductWithEmptyEANAndGoodProductShouldRaiseException() throws Exception {
        arrangeAncAct(Arrays.asList(new ProductSc("1235", 7.10) ,new ProductSc()));
        Assertions.assertThrowsExactly(NullPointerException.class, () -> scannerProduct.scanProduct());
    }

    @Test
    void testScanProductWithTwoProducts() throws Exception {
        arrangeAncAct(Arrays.asList(new ProductSc("23456", 12.50), new ProductSc("12345", 7.25)));
        Assertions.assertEquals(19.75, scannerProduct.scanProduct());
    }
}
