package org.example;

import org.example.interfaces.IGrid;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class GridConwayTest {
    private GridConway gridConway;

    @Mock
    private IGrid iGrid;

    void actAndArrange(boolean[][] grid){
        gridConway = new GridConway(iGrid);
        Mockito.when(iGrid.genereGrid()).thenReturn(grid);
    }

    @Test
    void testHaveATurnCeli1j1ShouldBeFalse() throws Exception {
        actAndArrange(new boolean[][]{{false, false, false}, {false, true, false}, {false, false, false}});
        Assertions.assertArrayEquals(new boolean[][]{{false, false, false}, {false, false, false}, {false, false, false}}, gridConway.haveATurn());
    }

    @Test
    void testHaveATurnCeli1j1Andj2i2ShouldBeFalse() throws Exception {
        actAndArrange(new boolean[][]{{false, false, false}, {false, true, false}, {false, false, true}});
        Assertions.assertArrayEquals(new boolean[][]{{false, false, false}, {false, false, false}, {false, false, false}}, gridConway.haveATurn());
    }

    @Test
    void testHaveATurnCeli1j1i0j1i2j1ShouldBeTrueAndOthersFalse() throws Exception {
        actAndArrange(new boolean[][]{{false, false, false}, {true, true, true}, {false, false, false}});
        Assertions.assertArrayEquals(new boolean[][]{{false, true, false}, {false, true, false}, {false, true, false}}, gridConway.haveATurn());
    }

    @Test
    void testHaveTwoTurnsShouldBeSameThatBegin() throws Exception {
        actAndArrange(new boolean[][]{{false, false, false}, {true, true, true}, {false, false, false}});
        actAndArrange(gridConway.haveATurn());
        Assertions.assertArrayEquals(new boolean[][]{{false, false, false}, {true, true, true}, {false, false, false}}, gridConway.haveATurn());
    }

    @Test
    void testAfterTwoTurnsAllCellsShouldBeFalse() throws Exception {
        actAndArrange(new boolean[][]{{false, false, true}, {false, true, false}, {true, false, false}});
        actAndArrange(gridConway.haveATurn());
        actAndArrange(gridConway.haveATurn());
        Assertions.assertArrayEquals(new boolean[][]{{false, false, false}, {false, false, false}, {false, false, false}}, gridConway.haveATurn());
    }

    @Test
    void testHaveTwoTurnsAllCellsShouldBeStable() throws Exception {
        actAndArrange(new boolean[][]{{true, true, false}, {true, false, false}, {false, false, false}});
        actAndArrange(gridConway.haveATurn());
        Assertions.assertArrayEquals(new boolean[][]{{true, true, false}, {true, true, false}, {false, false, false}}, gridConway.haveATurn());
    }
    @Test
    void testHaveATurnShouldResultAllCellsFalseAfter5Turns() throws Exception {
        actAndArrange(new boolean[][]{{true, false, false, false}, {false, true, true, true}, {false, false, false, false}});
        actAndArrange(gridConway.haveATurn());
        actAndArrange(gridConway.haveATurn());
        actAndArrange(gridConway.haveATurn());
        actAndArrange(gridConway.haveATurn());
        Assertions.assertArrayEquals(new boolean[][]{{false, false, false, false}, {false, false, false, false}, {false, false, false, false}}, gridConway.haveATurn());
    }
}
