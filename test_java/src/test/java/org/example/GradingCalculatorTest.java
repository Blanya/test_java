package org.example;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class GradingCalculatorTest {

    private GradingCalculator gradingCalculator;

    private char arrangeAndAct(int score, int attendancePercentage){
        gradingCalculator.setScore(score);
        gradingCalculator.setAttendancePercentage(attendancePercentage);
        return gradingCalculator.getGrade();
    }

    @BeforeEach
    void setUp(){
        gradingCalculator = new GradingCalculator();
    }

    //GivenWhenThen
    @Test
    void testGetGradeWithScore_95_andAttendance_90TheResultShouldBe_A(){
        char result = arrangeAndAct(95, 90);
        Assertions.assertEquals('A', result);
    }

    //ShouldWhen
    @Test
    void testGetGradeShouldBeBWhenScore85AndAttendance90(){
        char result = arrangeAndAct(85, 90);
        Assertions.assertEquals('B', result);
    }

    @Test
    void testGetGradeShouldBeCWhenScore65AndAttendance90(){
        char result = arrangeAndAct(65, 90);
        Assertions.assertEquals('C', result);
    }

    @Test
    void testGetGradeShouldBeBWhenScore95AndAttendance65(){
        char result = arrangeAndAct(95, 65);
        Assertions.assertEquals('B', result);
    }

    @Test
    void testGetGradeShouldBeFWhenScore95AndAttendance55(){
        char result = arrangeAndAct(95, 55);
        Assertions.assertEquals('F', result);
    }

    @Test
    void testGetGradeShouldBeFWhenScore65AndAttendance55(){
        char result = arrangeAndAct(65, 55);
        Assertions.assertEquals('F', result);
    }

    @Test
    void testGetGradeShouldBeFWhenScore50AndAttendance90(){
        char result = arrangeAndAct(50, 90);
        Assertions.assertEquals('F', result);
    }
}
