package org.example;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.*;

public class FibTest {

    private Fib fib;

    @Test
    void testGetFibSeriesShouldBeNotEmptyWhereRange1(){
        fib = new Fib(1);
        List<Integer> result = fib.getFibSeries();
        Assertions.assertTrue(result.size()>0);

        //autre possibilité
        //int res = fib.getFibSeries().size();
        //Assertions.assertTrue(res > 0);
    }

    @Test
    void testGetFibSeriesShouldBeListWith0WhenRange1(){
        fib = new Fib(1);
        List<Integer> result = fib.getFibSeries();
        Assertions.assertEquals(Arrays.asList(0), result);
    }

    @Test
    void testGetFibSeriesShouldContain3WhenRange6(){
        fib = new Fib(6);
        List<Integer> result = fib.getFibSeries();
        Assertions.assertTrue(result.contains(3));
    }

    @Test
    void testGetFibSeriesSizeShouldBe6WhenRange6(){
        fib = new Fib(6);
        List<Integer> result = fib.getFibSeries();
        Assertions.assertTrue(result.size() == 6);
    }

    @Test
    void testGetFibSeriesShouldNotContain4WhenRange6(){
        fib = new Fib(6);
        List<Integer> result = fib.getFibSeries();
        Assertions.assertFalse(result.contains(4));
    }

    @Test
    void testGetFibSeriesShouldBeListWith0With1With1With2With3With5(){
        fib = new Fib(6);
        List<Integer> result = fib.getFibSeries();
        Assertions.assertEquals(Arrays.asList(0, 1, 1, 2, 3, 5), result);
    }

    @Test
    void testGetFibSeriesShouldBeSortedWhereRange6(){
        fib = new Fib(6);
        List<Integer> result = fib.getFibSeries();
        Assertions.assertEquals(result.stream().sorted().toList(), result);
    }
}
