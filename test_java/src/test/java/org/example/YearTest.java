package org.example;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class YearTest {

    private Year year;

    //TDD => Premier Test faux => return 1 ds la méthode checkLeapYear
    // => True return 0
    // => 1er test => if (year%4000 == 0)
    // => 2ème test => else if (year%400 == 0)
    // => 3ème test => else if (year%4 == 0 && year%100 != 0)

    @BeforeEach
    void setUp(){
        year = new Year();
    }

    // REFRACTOR
    @Test
    void testGetYearShouldBeDivisibleBy400TrueWithYear2000(){
        Assertions.assertTrue(year.checkLeapYear(2000));
    }

    @Test
    void testGetYearShouldBeDivisibleBy4AndNotBy100TrueWithYear2012(){
        Assertions.assertTrue(year.checkLeapYear(2012));
    }

    @Test
    void testGetYearShouldBeDivisibleBy4000TrueWithYear4000(){
        Assertions.assertTrue(year.checkLeapYear(4000));
    }

    @Test
    void testGetYearShouldBeDivisibleBy400FalseWithYear1865(){
        Assertions.assertFalse(year.checkLeapYear(1865));
    }

    @Test
    void testGetYearShouldBeDivisibleBy4AndNotBy100FalseWithYear3000(){
        Assertions.assertFalse(year.checkLeapYear(3000));
    }

    @Test
    void testGetYearShouldBeDivisibleBy4000FalseWithYear1822(){
        Assertions.assertFalse(year.checkLeapYear(1822));
    }

}
