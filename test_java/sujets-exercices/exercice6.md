## Exercice 6 TDD avec Mock

Le but de l'exercice est de développer un prgm pour émuler le jeu du pendu. Le jeu a une classe LePendu, cette classe possède: 

- Une méthode pour générer un masque, cette méthode fait appel à un objet qui permet de générer de façon aléatoire un mot à trouver à partir d'un tableau de mot
- Une méthode pour tester si un char est ds le mot
- Une méthode pour tester si le joueur a gagné
