## Exercice révision 1 TDD
Créez une application simple pour scanner les codes-barres afin de vendre des produits.

Voici les conditions d'exécution de l'application :

- Le code-barres '12345' doit afficher le prix '$7.25'

- Le code-barres «23456» doit afficher le prix «12,50$»

- Le code-barres «99999» doit afficher «Erreur: code-barres introuvable»

- Le code-barres vide doit afficher «Erreur: code-barres vide»

Introduire un concept de commande où il est possible de numériser plusieurs produits. La commande affichera la somme des prix des produits scannés

Imaginez une structure des classes.
Codez les tests.
Implémentez la ou les méthodes.